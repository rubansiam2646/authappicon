import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Button,
} from 'react-native';

export default function page({navigation}) {
  return (
    <View>
      <View style={styles.container}>
        <Image style={styles.Logo} source={require('../assests/pic.png')} />
      </View>
      <View style={styles.cont2}>
        <Text style={styles.hello}>Hellooo!</Text>
        <Text style={styles.hello}>This is my First App!</Text>
      </View>
      <View style={styles.btns}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Login')}>
          <Text style={styles.b1}>log in</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Signup')}>
          <Text style={styles.b2}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Logo: {
    width: 200,
    height: 200,
  },
  container: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 200,
  },
  cont2: {
    marginTop: 150,

    alignItems: 'center',
    justifyContent: 'center',
  },
  hello: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },

  btns: {
    justifyContent: 'center',
    // alignItems:'center',
    marginTop: 40,
  },
  b1: {
    fontSize: 25,
    backgroundColor: 'blue',
    width: '50%',
    textAlign: 'center',
    marginLeft: 100,
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    color: 'white',
    fontWeight: '600',
    letterSpacing: 1,
  },
  b2: {
    fontSize: 25,
    backgroundColor: 'white',
    borderWidth: 1,
    width: '50%',
    textAlign: 'center',
    marginLeft: 100,
    marginTop: 20,
    borderRadius: 5,
    padding: 5,
    fontWeight: '600',
    letterSpacing: 1,
  },
});
