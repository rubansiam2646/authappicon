import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Page from './components/Page';
import Login from './components/Login';
import Signup from './components/Signup';
import Flat from './components/Flat';


const Stack = createNativeStackNavigator();

export default function 
() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        
        <Stack.Screen name="Page" component={Page} options={{headerShown:false}}/>
        <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
        <Stack.Screen name="Signup" component={Signup} options={{headerShown:false}} />
        <Stack.Screen name="Flat" component={Flat}/>

      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({})
 

